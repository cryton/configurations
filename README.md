# Configurations
This project contains configurations for Docker (Compose).

## Running Docker builds
To build and upload a specific image create a new tag with the name of the image (its directory name) and its version 
separated by a forward slash. For example: `ci-base/0.0.1`.


## Images

### ci-base
Contains prerequisites necessary for running tests in the gitlab CI.

```
ci-base/m.m.p
```

### production-base
Contains prerequisites necessary for building the Cryton tools.

```
production-base/m.m.p
```

### kali-prebuilt
Contains prerequisites necessary for the Cryton Worker.

```
kali-prebuilt/m.m.p
```

### metasploit-framework
Updated official MSF image that allows us to start MSFRPC.

```
metasploit-framework/m.m.p
```

### msf-official
Builds the official Docker image from the metasploit framework GitHub repository.

```
msf-official/m.m.p-commit
```

